import sys, os
import argparse

def parse_args():
    parser = argparse.ArgumentParser(description='GRO-Seq Pipeline')
    parser.add_argument("-f1", type = str, required = True, help = "Read 1st file" )
    parser.add_argument("-f2", type = str, help = "Read 2nd file (optional)" )
    parser.add_argument("-d1", dest = "firstdirectory", type = str, required = True, help = "1st directory: outputs will be placed here" )
    parser.add_argument("-d2", dest = "seconddirectory", type = str, required = True, help = "2nd directory: outputs will be placed here" )
    parser.add_argument("-d3", dest = "thirddirectory", type = str, required = True, help = "3rd directory: outputs will be placed here" )
    parser.add_argument("-o1", dest = "outputname1", type = str, required = True, help = "Output file name for the R1 file" )
    parser.add_argument("-o2", dest = "outputname2", type = str, help = "Output file name for the R2 file" )
    args = parser.parse_args()
    return args

def fq_to_bam(args,output_list):
    inputfile_list=[args.f1]
    if args.f2:
        inputfile_list.append(args.f2)
    for output,file in zip(output_list,inputfile_list):
        os.system("bowtie2 -x /storage/genomes/bowtie2_indexes/mm9 -U {0},{0} -p 20 --non-deterministic -S {1}/{2}.sam".format(file, args.firstdirectory, output))
        os.system("samtools view -bhS {0}/{1}.sam -o {0}/{1}.bam".format(args.firstdirectory, output))
        os.system("samtools sort {0}/{1}.bam {0}/{1}.sort".format(args.firstdirectory, output))
        os.system("mv {0}/{1}.sort.bam {0}/{1}.bam".format(args.firstdirectory, output))
        os.system("samtools index {0}/{1}.bam {0}/{1}.bam.bai".format(args.firstdirectory, output))
    return


def wig_convert(args,output_list):
    for output in output_list:
        os.system("nohup /opt/ActivePython-2.7/bin/macs2 callpeak -t {0}/{2}.bam -n {1}/{2} --nomode --keep-dup all --extsize 51 --nolambda -B --SPMR -g mm --verbose 0 --broad".format(args.firstdirectory, args.seconddirectory, output))
        os.system("nohup bedGraphToBigWig {0}/{1}_treat_pileup.bdg /home/zhoudu/Library/Samtool/chromInfo_mm9.txt {0}/{1}_treat_pileup.bw".format(args.seconddirectory, output))
        os.system("nohup samtools view -b -F 0x10 {0}/{1}.bam -o {0}/{1}.positive.bam".format(args.firstdirectory, output))
        os.system("nohup samtools view -b -f 0x10 {0}/{1}.bam -o {0}/{1}.negative.bam".format(args.firstdirectory, output))
        os.system("nohup /opt/ActivePython-2.7/bin/macs2 callpeak -t {0}/{2}.positive.bam -n {1}/{2}.positive --nomode --keep-dup all --extsize 51 --nolambda -B --SPMR -g mm --verbose 0 --broad".format(args.firstdirectory, args.seconddirectory, output))
        os.system("nohup /opt/ActivePython-2.7/bin/macs2 callpeak -t {0}/{2}.negative.bam -n {1}/{2}.negative --nomode --keep-dup all --extsize 51 --nolambda -B --SPMR -g mm --verbose 0 --broad".format(args.firstdirectory, args.seconddirectory, output))
        os.system("nohup /home/zhoudu/anaconda/bin/python /storage/researchers/jeff/GRO-Seq/Bin/filter.mm9.outer.py {0}/{1}.positive_treat_pileup.bdg plus > {0}/{1}.positive.bdg".format(args.seconddirectory, output))
        os.system("nohup /home/zhoudu/anaconda/bin/python /storage/researchers/jeff/GRO-Seq/Bin/filter.mm9.outer.py {0}/{1}.negative_treat_pileup.bdg minus > {0}/{1}.negative.bdg".format(args.seconddirectory, output))
        os.system("nohup bedGraphToBigWig {0}/{1}.positive.bdg /home/zhoudu/Library/Samtool/chromInfo_mm9.txt {0}/{1}.positive.bw".format(args.seconddirectory, output))
        os.system("nohup bedGraphToBigWig {0}/{1}.negative.bdg /home/zhoudu/Library/Samtool/chromInfo_mm9.txt {0}/{1}.negative.bw".format(args.seconddirectory, output))
    return

def homer(args,output_list):
    for output in output_list:
        os.system("/usr/local/homer/bin/makeTagDirectory {1}/{2} {0}/{2}.bam -genome mm9".format(args.firstdirectory, args.thirddirectory, output))
        os.system("/usr/local/homer/bin/findPeaks {0}/{1} -style groseq -o auto -uniqmap /usr/local/homer/data/uniqmap/mm9-uniqmap".format(args.thirddirectory,output))
    return

def convergent(args,output_list):
    for output in output_list:
        os.system("grep -v \"#\" {0}/{1}/transcripts.txt | awk '{{OFS=\"\t\"}}{{print $2, $3, $4, $1, $6, $5}}' > {0}/{1}/{1}.transcript.bed".format(args.thirddirectory, output))
        os.system("bedtools sort -i {0}/{1}/{1}.transcript.bed > see".format(args.thirddirectory, output))
        os.system("mv see {0}/{1}/{1}.transcript.bed".format(args.thirddirectory, output))
        os.system("grep -v \"+\" {0}/{1}/{1}.transcript.bed > {0}/{1}/{1}.transcripts.minus.bed".format(args.thirddirectory, output))
        os.system("grep \"+\" {0}/{1}/{1}.transcript.bed > {0}/{1}/{1}.transcripts.plus.bed".format(args.thirddirectory, output))
        os.system("bedtools multiinter -i {0}/{1}/{1}.transcripts.plus.bed {0}/{1}/{1}.transcripts.minus.bed |grep \"1,2\" |awk \'{{OFS=\"\\t\"}}{{ if (3-2>100){{print $1, $2, $3}} }}\' > {0}/{1}/{1}.transcripts.convergent.bed".format(args.thirddirectory, output))
    return

def gmean_cal(args,output_list):
    for output in output_list:
        os.system("awk '{{OFS=\"\\t\"}}{{printf \"%s\\t%s\\t%s\\t%s_%s_%s\\t.\\t.\\n\", $1, $2, $3, $1, $2, $3}}' {0}/{1}/{1}.transcripts.convergent.bed > {0}/{1}/{1}.transcripts.convergent.6.bed".format(args.thirddirectory, output))
        os.system("samtools view {0}/{2}.positive.bam | gfold count -annf BED -ann {1}/{2}/{2}.transcripts.convergent.6.bed -tag stdin -o {1}/{2}/{2}.positive.cnt".format(args.firstdirectory, args.thirddirectory, output))
        os.system("samtools view {0}/{2}.negative.bam | gfold count -annf BED -ann {1}/{2}/{2}.transcripts.convergent.6.bed -tag stdin -o {1}/{2}/{2}.negative.cnt".format(args.firstdirectory, args.thirddirectory, output))
        os.system("/home/zhoudu/anaconda/bin/python /storage/researchers/jeff/GRO-Seq/b.bin/COVT.gmean_cal.py {0}/{1}/{1} > {0}/{1}/{1}.gmean.cnt".format(args.thirddirectory, output))
        os.system("sort -gr -k 4 {0}/{1}/{1}.gmean.cnt > {0}/{1}/{1}.gmean.cnt.sort".format(args.thirddirectory, output))
        os.system("/home/zhoudu/anaconda/bin/python /storage/researchers/jeff/GRO-Seq/b.bin/Add.col.py {0}/{1}/{1}.gmean.cnt.sort > {0}/{1}/{1}.gmean.bed".format(args.thirddirectory, output))
    return

def GROSeqPL(args):
    os.system("mkdir -p {0} {1} {2}".format(args.firstdirectory,args.seconddirectory,args.thirddirectory))
    output_list=[args.outputname1]
    if args.f2:
        output_list.append(args.outputname2)
    fq_to_bam(args,output_list)
    wig_convert(args,output_list)
    homer(args,output_list)
    convergent(args,output_list)
    gmean_cal(args,output_list)

def main():
    args = parse_args()
    GROSeqPL(args)

main()
